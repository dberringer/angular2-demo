/// <reference path="typings/angular2/angular2.d.ts" />

import {Component, View, bootstrap, For, If} from 'angular2/angular2';

// function MyTodoService () {
// 	this.todos = ['Foo', 'Bar', 'Bass'];
// }


// Sub-component
// @Component({
// 	selector: 'sub-component'
// })
// @View({
// 	template: `<h1>Hi, {{content}}</h1>`
// })
// // Component Controller
// class MySubComponent {

// 	constructor() {
// 		this.content = 'I am the sub-component';
// 	}
// }



// ---------------------------------------------------------- //


// Main Component
@Component({
	selector: 'my-app'
	// injectables: [MyTodoService]
})
@View({
	template: `<h1>Hello {{myName}}</h1>`
	// templateUrl: 'template.html',
	// directives: [For, If]  // , MySubComponent]
})
// Component Controller
class MyAppComponent {
	myName: string;
	todos;

	constructor() {
	// constructor(todoSvc:MyTodoService) {
		this.myName = 'Doug';
		// this.todos = ['Foo', 'Bar', 'Bass'];
		// this.todos = todoSvc.todos;
	}

	// addTodo(newTodo) {
	// 	this.todos.push(newTodo.value);
	// 	newTodo.value = '';
	// }

	// clearTodos() {
	// 	this.todos = [];
	// }
}





bootstrap(MyAppComponent);